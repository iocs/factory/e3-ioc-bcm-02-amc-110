require essioc
require adsis8300bcm

# set macros
epicsEnvSet("CONTROL_GROUP", "PBI-BCM02")
epicsEnvSet("AMC_NAME",      "Ctrl-AMC-110")
epicsEnvSet("AMC_DEVICE",    "/dev/sis8300-5")
epicsEnvSet("EVR_NAME",      "PBI-BCM02:Ctrl-EVR-101:")

epicsEnvSet("P",        "$(CONTROL_GROUP):")
epicsEnvSet("R",        "$(AMC_NAME):")
epicsEnvSet("PREFIX",   "$(P)$(R)")

# set BCM generic channel names
iocshLoad("bcm-channels.iocsh")

# set BCM specific channel names
epicsEnvSet("SYSTEM1_PREFIX",  "DTL-030:PBI-BCM-001:")
epicsEnvSet("SYSTEM1_NAME",    "PBI-BCM02#1 - DTL-030")
epicsEnvSet("SYSTEM1_ARCHIVER","")

epicsEnvSet("SYSTEM2_PREFIX",  "DTL-040:PBI-BCM-001:")
epicsEnvSet("SYSTEM2_NAME",    "PBI-BCM02#2 - DTL-040")
epicsEnvSet("SYSTEM2_ARCHIVER","")

epicsEnvSet("SYSTEM3_PREFIX",  "DTL-050:PBI-BCM-001:")
epicsEnvSet("SYSTEM3_NAME",    "PBI-BCM02#3 - DTL-050")
epicsEnvSet("SYSTEM3_ARCHIVER","")








# load BCM data acquistion
iocshLoad("$(adsis8300bcm_DIR)/bcm-main.iocsh", "CONTROL_GROUP=$(CONTROL_GROUP), AMC_NAME=$(AMC_NAME), AMC_DEVICE=$(AMC_DEVICE), EVR_NAME=$(EVR_NAME)")

# load common module
iocshLoad $(essioc_DIR)/common_config.iocsh

# custom autosave in order to have Lut ID PVs restored before the others
afterInit("makeAutosaveFileFromDbInfo('$(AS_TOP)/$(IOCDIR)/req/lutIDs.req','autosaveFieldsLutIDs')")
afterInit("create_monitor_set("lutIDs.req",5)")
afterInit("fdbrestore("$(AS_TOP)/$(IOCDIR)/save/lutIDs.sav")")
afterInit("epicsThreadSleep(2)")

afterInit("fdbrestore("$(AS_TOP)/$(IOCDIR)/save/settings.sav")")

# call iocInit
iocInit

date

